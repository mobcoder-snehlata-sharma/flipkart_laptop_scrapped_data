import requests
from bs4 import BeautifulSoup
import pandas as pd


url = "https://www.flipkart.com/search?q=laptop&sid=6bo%2Cb5g&as=on&as-show=on&otracker=AS_QueryStore_OrganicAutoSuggest_1_3_na_na_na&otracker1=AS_QueryStore_OrganicAutoSuggest_1_3_na_na_na&as-pos=1&as-type=RECENT&suggestionId=laptop%7CLaptops&requestId=583bd998-6c41-4dd2-b240-3ff375954136&as-backfill=on"
request_page = requests.get(url)

soup_content = BeautifulSoup(request_page.content, "html.parser")

page_title = soup_content.title.text

product_name=[]             
prices=[]               
ratings=[]              
processors = []               
os = []                 
ram = []      
ssd = [] 

for data in soup_content.findAll('div',class_='_3pLy-c row'):

    names=data.find('div', attrs={'class':'_4rR01T'})
    price=data.find('div', attrs={'class':'_30jeq3 _1_WHN1'})
    rating=data.find('div', attrs={'class':'_3LWZlK'})
    specification = data.find('div', attrs={'class':'fMghEO'})
    
    for each in specification:
        col=each.find_all('li', attrs={'class':'rgWa7D'})
        processor =col[0].text
        ram_ = col[1].text
        os_ = col[2].text
        ssd_ = col[3].text


    product_name.append(names.text)
    prices.append(price.text)
    processors.append(processor)
    os.append(os_) 
    ram.append(ram_) 
    ssd.append(ssd_)
    ratings.append(rating.text) 

df=pd.DataFrame({'Product Name':product_name,'Processor':processors,'SSD_HDD Capacity':ssd,'RAM and RAM type':ram,"OS":os,'Price':prices,'Rating':ratings})
df.head(10)

with pd.ExcelWriter("filpkart_laptops_data.xlsx") as df_writer:
    df.to_excel(df_writer, sheet_name="Laptops")